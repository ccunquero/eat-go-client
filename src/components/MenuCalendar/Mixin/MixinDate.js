export function addDays(date, days) {
    date.setDate(date.getDate() + days);
    return date;
}
export function getCurrentDate() {
    return new Date(window.localStorage.getItem("CURRENT_DATE"))

}