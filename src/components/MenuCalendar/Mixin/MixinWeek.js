import * as mixinDate from './MixinDate'
export default {
    data() {
        return {
            currentDate: new Date(window.localStorage.getItem("CURRENT_DATE")),

        }
    },
    methods: {

        buildArrayWeek() {
            let currentDate = this.currentDate

            let arrayWeek = []
            let firstDay = this.getMonday()
            let lastDay = mixinDate.addDays(this.getMonday(currentDate), 4)

            for (let i = 0; i < lastDay.getDay(); i++) {
                arrayWeek.push(mixinDate.addDays(new Date(firstDay), i))

            }
            return arrayWeek

        },
        getMonday() {
            let d = mixinDate.getCurrentDate()
            d = new Date(d);
            var day = d.getDay(),
                diff = d.getDate() - day + (day == 0 ? -6 : 1);
            return new Date(d.setDate(diff));
        },
        getFriday() {
            let currentDate = mixinDate.getCurrentDate()
            return mixinDate.addDays(this.getMonday(currentDate), 4)

        },
        getNameDayWeek(date) {
            var days = ['D', 'L', 'M', 'M', 'J', 'V', 'S'];
            return days[date.getDay()];
        },
        getNameMonth(date, arrayWeek) {

            const monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
            ];

            const monthNameAbbreviation = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                "Julio", "Agosto", "Sep.", "Oct.", "Nov.", "Dic."
            ];

            let firstDay = arrayWeek[0]
            let lastDay = arrayWeek[arrayWeek.length - 1]
            let nameMonth

            if (firstDay.getMonth() != lastDay.getMonth()) {
                nameMonth = monthNameAbbreviation[firstDay.getMonth()] + " - " + monthNameAbbreviation[lastDay.getMonth()]

            } else {
                nameMonth = monthNames[date.getMonth()]

            }

            return nameMonth
        },
        backWeek() {
            let currentDate = this.currentDate

            this.currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - 7);
            window.localStorage.setItem("CURRENT_DATE", this.currentDate)
            return this.currentDate

        },
        nextWeek() {
            let currentDate = this.currentDate
            this.currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 7);
            window.localStorage.setItem("CURRENT_DATE", this.currentDate)
            return this.currentDate

        },

    },
}