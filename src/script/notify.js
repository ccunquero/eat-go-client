export function showNotify(context_q, code, message) {
    if (code == 200) {

        context_q.notify({
            message: message,
            type: 'positive',
            color: 'positive',
            actions: [{
                icon: 'close',
                handler: () => {}
            }],
            onDismiss() {}
        })

    } else {
        context_q.notify({
            message: message,
            type: 'negative',
            color: 'negative',
            actions: [{
                icon: 'close',
                handler: () => {}
            }],
            onDismiss() {}
        })
    }
}
export function showNotifyError(context_q) {
    context_q.notify({
        message: `Por favor revise los campos de nuevo`,
        type: 'negative',
        color: 'nevative',
        actions: [{
            icon: 'close',
            handler: () => {}
        }],
        onDismiss() {}
    })
}
export function showNotifyErrorSrv(context_q) {
    context_q.notify({
        message: `Ha ocurrido un error en el servidor`,
        type: 'negative',
        color: 'nevative',
        actions: [{
            icon: 'close',
            handler: () => {}
        }],
        onDismiss() {}
    })
}
export function showNotifySuccess(context_q, messageNotify) {
    context_q.notify({
        message: messageNotify,
        type: 'positive',
        color: 'positive',
        actions: [{
            icon: 'close',
            handler: () => {}
        }],
        onDismiss() {}
    })
}
export function showNotifyInfo(context_q, messageNotify) {
    context_q.notify({
        message: messageNotify,
        type: 'info',
        color: 'primary',
        actions: [{
            icon: 'close',
            handler: () => {}
        }],
        onDismiss() {}
    })
}