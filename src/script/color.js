export const colorMen = {
    "blue": "#2196F3",
    "teal": "#009688",
    "blue_gray": "#455A64",
    "green": "#4CAF50",
    "orange": "#FF9800",
    "red": "#f44336",
    "brown": "#795548",
    "deep_orange": "#FF5722",
    "amber": "#FFC107",
    "purple": "#9C27B0",
    "deep_purple": "#673AB7",
    "pink": "#E91E63",

}
export const colorWonan = {
    "pink": "#E91E63",
    "amber": "#FFC107",
    "cyan": "#00BCD4",
    "lime": "#CDDC39",
    "purple": "#9C27B0",
    "deep_purple": "#673AB7",
    "ligth_green": "#8BC34A",
    "ligth_blue": "#03A9F4",
}