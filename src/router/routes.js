const routes = [{
            path: '/Home',
            meta: {
                public: false,
                requiresAuth: true
            },
            component: () =>
                import ('components/Home/Home.vue'),
            children: [

            ]
        },
        {
            path: '/Activity',
            meta: {
                public: false,
                requiresAuth: true
            },
            component: () =>
                import ('components/Activity/Activity.vue'),
            children: [

            ]
        },
        {
            path: '/Transfer',
            meta: {
                public: false,
                requiresAuth: true,
                pin: true
            },
            component: () =>
                import ('components/Transfer/TransferList.vue'),
            children: [

            ]
        },

        {
            path: '/QR',
            meta: {
                public: false,
                requiresAuth: true
            },
            component: () =>
                import ('components/QR/QR.vue'),
            children: [

            ]
        },
        {
            path: '/MyAccount',
            meta: {
                public: false,
                requiresAuth: true,
                pin: true
            },
            component: () =>
                import ('components/MyAccount/Config.vue'),
            children: [

            ]
        },
        {
            path: '/Card',
            meta: {
                public: false,
                requiresAuth: true
            },
            component: () =>
                import ('components/Card/CardList.vue'),
            children: [

            ]
        },
        {
            path: '/Money',
            meta: {
                public: false,
                requiresAuth: true
            },
            component: () =>
                import ('components/Money/AvailableMoney.vue'),
            children: [

            ]
        },
        {
            path: '/Login',
            meta: {
                public: true,
                requiresAuth: false
            },
            component: () =>
                import ('components/LoginUser/LoginRegister.vue'),
            children: [

            ]
        },
        {
            path: '/Billing',
            meta: {
                public: false,
                requiresAuth: true
            },
            component: () =>
                import ('components/MyAccount/BillingList.vue'),
            children: [

            ]
        },
        {
            path: '/MenuCalendar',
            meta: {
                public: false,
                requiresAuth: true,
                pin: false
            },
            component: () =>
                import ('components/MenuCalendar/Main.vue'),
            children: [

            ]
        },

        {
            path: '/RequestAmount',
            meta: {
                public: false,
                requiresAuth: true
            },
            component: () =>
                import ('components/RequestAmount/AmountList.vue'),
            children: [

            ]
        },
        {
            path: '/ErrorLoad',
            meta: {
                public: false,
                requiresAuth: true
            },
            component: () =>
                import ('pages/ErrorLoad.vue'),
            children: [

            ]
        },

    ]
    // Always leave this as last one
if (process.env.MODE !== 'ssr') {
    routes.push({
        path: '*',
        meta: {
            public: false
        },
        component: () =>
            import ('components/Home/Home.vue')
    })
}

export default routes