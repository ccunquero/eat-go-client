const Menu = [{
        title: ' Inicio',
        icon: 'fa fa-home',
        component: 'Home'
    },
    {
        title: ' Actividades',
        icon: 'fas fa-dollar-sign',
        component: 'Activity'
    },
    {
        title: ' Transferencias',
        icon: 'fa fa-exchange-alt',
        component: 'Transfer'
    },
    {
        title: ' Código',
        icon: 'fa fa-qrcode',
        component: 'QR'
    },
    {
        title: ' Mi Cuenta',
        icon: 'fas fa-id-card',
        component: 'MyAccount'
    },
    {
        title: ' Tarjetas',
        icon: 'fas fa-credit-card',
        component: 'Card'
    },
    {
        title: ' Saldo Disponible',
        icon: 'fas fa-money-bill-alt',
        component: 'Money'
    },
    {
        title: ' Asignación Menú',
        icon: 'far fa-calendar-check',
        component: 'MenuCalendar'
    },
    {
        title: ' Ingreso Saldo',
        icon: 'fas fa-money-check-alt',
        component: 'RequestAmount'
    },
    {
        title: ' Salir',
        icon: 'fas fa-sign-out-alt',
        component: 'Login'
    },
]

export default Menu;