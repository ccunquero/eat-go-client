import * as mixinDate from './MixinDate'
export default {
    data() {
        return {
            currentDate: new Date(window.localStorage.getItem("CURRENT_DATE")),
            numDays: 0
        }
    },
    methods: {
        test() {
            // alert(DateTime.fromISO(new Date().toISOString() ).setLocale('gt').toFormat('dd LLLL yyyy'))

        },
        buildArrayMonth() {
            this.numDays = 0
            let arrayMonth = []
            var firstDay = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), 1);
            var lastDay = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth() + 1, 0);

            let currentDate = firstDay
            for (let i = 0; i < lastDay.getDate(); i++) {
                currentDate = mixinDate.addDays(new Date(firstDay), i)

                if (!this.validNameDate(currentDate, 0) &&
                    !this.validNameDate(currentDate, 6)) {
                    arrayMonth.push(currentDate)
                    this.numDays++
                }

            }
            return this.setSpaceArrayMonth(arrayMonth)

        },
        setSpaceArrayMonth(arrayMonth) {
            let firstDay = arrayMonth[0].getDay()
            if (firstDay != 0 && firstDay != 6) {
                for (let i = 1; i < firstDay; i++) {
                    arrayMonth.unshift(null)
                }
            }
            return arrayMonth
        },

        validNameDate(date, nameDay) {
            let flag = false
            if (date.getDay() == nameDay) {
                flag = true
            }
            return flag
        },
        getDayFormat(date) {
            let day = new Date(date).getDate()

            return day
        },
        getNamesDays() {
            let arrayNames = []
            arrayNames.push('L')
            arrayNames.push('M')
            arrayNames.push('M')
            arrayNames.push('J')
            arrayNames.push('V')
            return arrayNames
        },
        getCurrentNameMonth(date) {

            const monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                "Julio", "Agosto", "Septiembre", "Obtubre", "Noviembre", "Diciembre"
            ];
            let nameMonth
            if (!date) {
                let currentDate = this.currentDate
                nameMonth = monthNames[this.getFirstDayWeek(currentDate).getMonth()] + " " + this.currentDate.getFullYear()
                    //   nameMonth =  monthNames[currentDate.getMonth()] + " "+ this.currentDate.getFullYear()
            } else {
                nameMonth = monthNames[date.getMonth()]
            }

            return nameMonth
        },
        nextMonth() {

            let date = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth() + 1, 1);
            if (date.getDay() == 6) {
                date = mixinDate.addDays(date, 2)

            }
            this.currentDate = date
            window.localStorage.setItem("CURRENT_DATE", this.currentDate)
        },
        backMonth() {
            let date = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth() - 1, 1);
            if (date.getDay() == 6) {

                date = mixinDate.addDays(date, 2)
            }
            this.currentDate = date
            window.localStorage.setItem("CURRENT_DATE", this.currentDate)
        },
        getCurretnNameDay(date) {
            return this.getDayFormat(date) + " de " + this.getCurrentNameMonth(date)
        },
        getNumDaysMonth() {
            return this.numDays
        },
        getFirstDayWeek(date) {
            let d = new Date(date);
            var day = d.getDay(),
                diff = d.getDate() - day + (day == 0 ? -6 : 1);
            return new Date(d.setDate(diff));
        },

    },
}