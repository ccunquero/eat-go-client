import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuelidate from 'vuelidate'
import routes from './routes'
import VueQRCodeComponent from 'vue-qrcode-component'
import VueTheMask from 'vue-the-mask'
import GSignInButton from 'vue-google-signin-button'
import axios from 'axios'
import VueAxios from 'vue-axios'
import mixin from '../script/mixin'

Vue.use(VueAxios, axios)
Vue.mixin(mixin)
Vue.use(GSignInButton)
Vue.use(VueTheMask)
Vue.component('qr-code', VueQRCodeComponent)
Vue.use(Vuelidate)
Vue.use(VueRouter)

 
/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })
  // router gards
  Router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
      // this route requires auth, check if logged in
      // if not, redirect to login page.
      var user = window.localStorage.getItem("USER_ID");
      if (user==null || user == undefined) {
        next({
          path: '/Login',
          query: { redirect: to.fullPath },
        });
      } else {
        next();
      }
    } else {
      next(); // make sure to always call next()!
    }
 });

  Router.afterEach((to, from) => {
    // ...
    
  });


  return Router
}
